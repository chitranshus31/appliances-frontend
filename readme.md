## Appliance Management

Clone this repository

After clonig run the following command in your terminal:

```bash
cd appliances-frontend/
```

To install all the dependencies run the following command

```bash
npm install
```

### Make sure the node server for appliance-backend is running

Now to run the application

```bash
npm start
```

If you face errors while running the application, run the following command

```bash
rm -rf node_modules && npm install
```

Now you can visit the app on http://localhost:3000 in your browser

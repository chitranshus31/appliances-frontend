
import {updateAppliance, createAppliance } from "../../utils/api/appliances.api";
export default (async function submit(values) {
  // return console.log(values);
  const id = window.location.pathname.split('/')[2];
  if(id){
    updateAppliance(id, values).then((res)=>console.log(res.data))
    window.location.assign('/');
    return;
  }
  const aaa = createAppliance(values).then(res=>console.log(res.data));
  // console.log(aaa.data);
  // window.location.assign('/');
});


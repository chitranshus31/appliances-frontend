import React from "react";
import { shallow } from "enzyme";
import Form from "./Form";
import { reducer as formReducer } from "redux-form";
import { createStore, Store, combineReducers } from "redux";
import { Provider } from "react-redux";

const spy = jest.fn();

const rootReducer = combineReducers({
  form: formReducer,
});
let store;

describe("<Form />", () => {
  beforeEach(() => {
    store = createStore(rootReducer);
  });

  it("should render form component", () => {
    const wrapper = shallow(
      <Provider store={store}>
        <Form />
      </Provider>
    );
    expect(wrapper).toBeTruthy();
  });
});

import React from "react";

import { shallow } from "enzyme";
import Form from "./Form";
import AddAppliances from "./AddAppliances";

describe("<AppliancesDashboard />", () => {
  it("should render add appliance form", () => {
    const wrapper = shallow(<AddAppliances />);
    expect(wrapper).toBeTruthy();
  });
});

import React, { Component } from "react";
import { Provider } from "react-redux";
import store from "../../../src/store/addAppliances/addAppliance.reducer";
import Form from "./Form";
import SweetAlert from "sweetalert-react";
import "sweetalert/dist/sweetalert.css";
import {
  updateAppliance,
  createAppliance,
} from "../../utils/api/appliances.api";

class AddAppliances extends Component {
  // const { id } = useParams();
  state = {
    show: false,
    title:'',
  };
  submit = (values) => {
    const id = window.location.pathname.split("/")[2];

    if (id) {
      updateAppliance(id, values).then((res) => console.log(res.data));
      window.location.assign("/");
      return;
    }
    createAppliance(values).then((res) => {
      if (res.data.includes("already exists")) {
        this.setState({ show: true });
        this.setState({ title: res.data });
      } else {
        window.location.assign("/");
      }
    });
  };

  render() {
    return (
      <Provider store={store}>
        <div>
          {/* <h2>{id?'Edit Appliance':'Add New Appliance'}</h2> */}
          <h2>Message</h2>
          <Form onSubmit={this.submit} />
          <SweetAlert
            type="warning"
            show={this.state.show}
            title={this.state.title}
            text="Please choose another name"
            onConfirm={() => {
              this.setState({ show: false });
            }}
          />
        </div>
      </Provider>
    );
  }
}

export default AddAppliances;

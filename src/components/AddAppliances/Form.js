import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { load as loadAccount } from "../../store/addAppliances/account";
import { getAppliancesbyId } from "../../utils/api/appliances.api";

/**
 * Setting validation for form fields
 */
const required = (value) => (value ? undefined : "Required");
const maxLength = (max) => (value) =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined;
const maxLength15 = maxLength(20);

/**
 * Setting up fields to render redux form
 */
const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning },
}) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type} />
      {touched &&
        ((error && <span style={{color:'red'}}>{error}</span>) ||
          (warning && <span style={{color:'red'}}>{warning}</span>))}
    </div>
  </div>
);

let Form = (props) => {
  const { id } = useParams();
  const { handleSubmit, load, pristine, reset, submitting } = props;

  /*
   * Fetch appliance detail by id when component gets rendered and there is an id in url ( To edit the appliance)
   */
  useEffect(() => {
    if (id) {
      getAppliancesbyId(id).then((res) => load(res.data.appliance[0]));
    } else {
      load();
    }
  }, [id, load]);

  return (
    <div className="ui raised segment">
      <form className="ui form" onSubmit={handleSubmit} style={{width: '97%'}}>
        <div className="field">
          <label>Appliance Name</label>
          <div>
            <Field
              name="name"
              type="text"
              placeholder="Name"
              component={renderField}
              validate={[required, maxLength15]}
            />
          </div>
        </div>
        <div className="field">
          <label>Place</label>
          <div>
            <Field
              name="place"
              component={renderField}
              type="text"
              placeholder="Place"
              validate={[required, maxLength15]}
            />
          </div>
        </div>
        <div className="field">
          <label>Manufacturer</label>
          <div>
            <Field
              name="manufacturer"
              component={renderField}
              type="text"
              placeholder="Manufacturer"
              validate={[required, maxLength15]}
            />
          </div>
        </div>
        <div className="field">
          <label>State</label>
          <div>
            <label>
              <Field
                name="state"
                component={renderField}
                type="radio"
                value="on"
              />
              ON
            </label>
            <label>
              <Field
                name="state"
                component={renderField}
                type="radio"
                value="off"
              />
              OFF
            </label>
          </div>
        </div>

        <div className="field">
          <button
            className="ui primary basic button"
            type="submit"
            disabled={pristine || submitting}
          >
            Submit
          </button>
          <button
            className="ui negative basic button"
            type="button"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Undo Changes
          </button>
        </div>
      </form>
    </div>
  );
};

/*
 * reduxForm() will read the initialValues prop provided by connect()
 */
Form = reduxForm({
  form: "initializeFromState",
  enableReinitialize: true,
})(Form);

/*
 * Connect to reducers
 */
Form = connect(
  (state) => ({
    initialValues: state.account.data,
  }),
  { load: loadAccount } // bind account loading action creator
)(Form);

export default Form;

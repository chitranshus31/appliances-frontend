import React from "react";
import { shallow } from "enzyme";
import Appliances from "./Appliances";
const name = "Washing Machine";
const place = "Laundry area";
describe("<Appliances />", () => {

    it("renders without crashing", () => {
    const wrapper = shallow(<Appliances />);
    expect(wrapper).toBeTruthy();
  });

  it("renders passed name and icon props correctly", () => {
    
    const wrapper = shallow(<Appliances name={name} place={place} />);
    expect(wrapper.text()).toContain(name);
    expect(wrapper.text()).toContain(place);
  });

  it("should have the edit button", () => {
    const wrapper = shallow(<Appliances name={name} place={place} />);
    const findButton = wrapper.find("#edit-button");
    expect(findButton.length).toEqual(1);
  });
  
  it("should have the delete button", () => {
    const wrapper = shallow(<Appliances name={name} place={place} />);
    const findButton = wrapper.find(".negative");
    findButton.simulate('click');
    expect(findButton.length).toEqual(1);
  });
  
  it("should have the toggle switch button", () => {
    const wrapper = shallow(<Appliances name={name} place={place} />);
    const findButton = wrapper.find(".checkbox");
    expect(findButton.length).toEqual(1);
  });

});

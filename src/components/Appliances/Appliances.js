import React, { Component } from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";
import { deleteAppliance, updateAppliance } from "../../utils/api/appliances.api";
import SweetAlert from "sweetalert-react";
import "sweetalert/dist/sweetalert.css";
import classes from "./Room.module.scss";
class Appliances extends Component {
  state = {
    show: false,
    redirect: false,
    checked: true,
  };

  /**
   * Redirect to edit page on button click
   */
  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = (id) => {
    if (this.state.redirect) {
      let uri = "/edit-appliance/" + id;
      return <Redirect to={uri} />;
    }
  };

  /**
   * function to change the state of appliance and update in database (ON or Off)
   */ 
  toggle = () => {
    let id = this.props.id;
    let value = (this.state.checked) ? 'off': 'on';
    // console.log(id, {state: value});
    updateAppliance(id, {state: value}).then((res)=>console.log(res))
    this.setState((prevState) => ({ checked: !prevState.checked }))
  }

  /**
   * Fetch all the appliances from database when component gets mounted
   */ 
  componentDidMount() {
    (this.props.state === 'on') ? this.setState({checked: true}): this.setState({checked: false});
  }
 
  render() {
    return (
      <div className={classes.Room} id={this.props.id}>
        <div>
          <div className={classes.Title}>{this.props.name}</div>

          <div>{this.props.place}</div>
          <div>Manufacturer → {this.props.manufacturer}</div>
          <div className="ui buttons">
            {this.renderRedirect(this.props.id)}
            <button id="edit-button" onClick={this.setRedirect} className="ui positive button">
              Edit
            </button>
            <div className="or"></div>
            <button
              onClick={() => this.setState({ show: true })}
              className="ui button negative"
            >
              Delete
            </button>
            <SweetAlert
              type="warning"
              show={this.state.show}
              title="Do you want to delete this appliance?"
              showCancelButton
              onConfirm={() => {
                deleteAppliance(this.props.id);
                this.setState({ show: false });
                window.location.reload(false)   // Remove it later
              }}
              onCancel={() => {
                this.setState({ show: false });
              }}
            />
          </div>
        </div>
        <div style={{marginLeft:'auto'}}>
        <div className="ui left floated compact segment" onClick={this.toggle}>
          Off
          <div className="ui fitted toggle checkbox" style={{left:'5px', top:'5px', width: '4.5rem'}} >
            <input type="checkbox" checked={this.state.checked} />
            <label></label>
          </div>
          On
        </div>
        </div>
      </div>
    );
  }
}

/**
 * Type Casting
 */ 
Appliances.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  place: PropTypes.string,
  renderRedirect: PropTypes.func,
  setRedirect: PropTypes.func,
  state:PropTypes.string,
  manufacturer:PropTypes.string,
};

export default Appliances;

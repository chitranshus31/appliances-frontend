import React from "react";
import { Route, Switch } from "react-router-dom";

import "./App.module.scss";
import Layout from "./hoc/Layout/Layout";
import AppliancesDashboard from "./containers/AppliancesDashboard/AppliancesDashboard";
import AddAppliances from "../src/components/AddAppliances/AddAppliances"

function App() {
  return (
    <Layout>
      <Switch>
        <Route path="/" exact component={AppliancesDashboard} />
        <Route path="/add-appliances/" exact component={AddAppliances} />
        <Route path="/edit-appliance/:id" exact component={AddAppliances} />
      </Switch>
    </Layout>
  );
}

export default App;

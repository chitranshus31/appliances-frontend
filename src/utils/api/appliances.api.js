import axios from "./axios";

export const getAppliancesApi = () => {
  return axios.get("/appliances");
};
export const getAppliancesbyId = (id) => {
  return axios.get("/appliances/"+id);
};
export const updateAppliance = (id, values) => {
  return axios.post("/update/"+id, values);
};
export const createAppliance = (values) => {
  return axios.post("/create", values);
};
export const deleteAppliance = (id) => {
  return axios.get("/delete/"+id);
};

import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import { reducer } from 'react-redux-sweetalert'; 

const rootReducer = combineReducers({
    sweetalert: reducer,
  });
  
  const logger = createLogger();
  
  const store = createStore(
    rootReducer,
    applyMiddleware(thunk, logger),
  );
  export default store
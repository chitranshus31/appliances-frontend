import {
  FETCH_APPLIANCES_START,
  FETCH_APPLIANCES_SUCCESS,
  FETCH_APPLIANCES_FAILED
} from "./appliances.actiontypes";

const initialState = {
  appliances: {}
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_APPLIANCES_START:
      return { ...state };

    case FETCH_APPLIANCES_SUCCESS:
      return {
        ...state,
        appliances: payload.appliances
      };

    case FETCH_APPLIANCES_FAILED:
      return { ...state };
    default:
      return state;
  }
};

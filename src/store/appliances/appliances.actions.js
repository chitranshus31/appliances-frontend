import {
  FETCH_APPLIANCES_START,
  FETCH_APPLIANCES_SUCCESS,
  FETCH_APPLIANCES_FAILED
} from "./appliances.actiontypes";
import { getAppliancesApi } from "../../utils/api/appliances.api";
import { showErrorModal } from "../ui/ui.actions";

export const fetchAppliances = () => dispatch => {
  dispatch(fetchAppliancesStart());

  getAppliancesApi()
    .then(response => dispatch(fetchAppliancesSuccess(response.data.appliances)))
    
    .catch(error => {

      // This to mock an error response
      const errorResponse = {
        message: "Error while getting the appliances data, Please check if node server is running"
      };
      
      dispatch(fetchAppliancesFailed(errorResponse));
      dispatch(showErrorModal(errorResponse));
    });
};

export const fetchAppliancesStart = payload => ({
  type: FETCH_APPLIANCES_START
});

export const fetchAppliancesSuccess = appliances => ({
  type: FETCH_APPLIANCES_SUCCESS,
  payload: {
    appliances
  }
});

export const fetchAppliancesFailed = error => ({
  type: FETCH_APPLIANCES_FAILED,
  payload: {
    error
  }
});

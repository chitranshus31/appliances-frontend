import React from "react";
import { shallow } from "enzyme";
import { AppliancesDashboard } from "./AppliancesDashboard";
import Appliances from "../../components/Appliances/Appliances";

describe("<AppliancesDashboard />", () => {
  it("should render appliance dashboard without crash", () => {
    const wrapper = shallow(<AppliancesDashboard />);
    expect(wrapper).toBeTruthy();
  });

  it("calls fetch appliance api on mount", () => {
    jest.spyOn(AppliancesDashboard.prototype, "componentDidMount");
    const fetchAppliances = jest.fn();

    shallow(<AppliancesDashboard fetchAppliances={fetchAppliances} />);

    const ComponentDidMountLifeCycleCount =
      AppliancesDashboard.prototype.componentDidMount.mock.calls.length;

    expect(ComponentDidMountLifeCycleCount).toBe(1);
    expect(fetchAppliances).toBeCalled();
  });

  it("renders a <Appliance /> component when a appliances prop is passed", () => {
    const Mockappliance = [{
      created: "2020-12-29T02:56:37.984Z",
      manufacturer: "Test",
      name: "Just a test",
      place: "Test",
      slug: "just-a-test",
      state: "on",
      __v: 0,
      _id: "5fea9ae59941f9a203fd9e5d",
    }];
    const wrapper = shallow(<AppliancesDashboard appliances={Mockappliance} />);
    expect(wrapper.find(Appliances)).toHaveLength(1);
  });

});

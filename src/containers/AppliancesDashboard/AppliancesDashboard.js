import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchAppliances } from "../../store/appliances/appliances.actions";
import Appliances from "../../components/Appliances/Appliances";

import classes from "./RoomsDashboard.module.scss";

export class AppliancesDashboard extends Component {
  static propTypes = {
    fetchAppliances: PropTypes.func,
    appliances: PropTypes.array
  };

  componentDidMount() {
    if(this.props.fetchAppliances){
      this.props.fetchAppliances();
    }
  }
  render() {
    if (!this.props.appliances) return null
    return (
      <div className={classes.Row}>
        {Object.entries(this.props.appliances).map(applianceData => {
          const applianecId = applianceData[0];
          const appliance = applianceData[1];
          return (
            <div
              data-test={`room-card-${applianecId}`}
              key={applianecId}
              className={classes.Column}
            >
              <Appliances
                id={appliance._id}
                name={appliance.name}
                place={appliance.place}
                state={appliance.state}
                manufacturer={appliance.manufacturer}
              />
            </div>
          );
        })}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  appliances: state.appliances.appliances
});

const mapDispatchToProps = {
  fetchAppliances
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppliancesDashboard);
